package org.welch;

/**
 * Created with IntelliJ IDEA.
 * User: matt
 * Date: 10/08/14
 * Time: 12:55
 * To change this template use File | Settings | File Templates.
 */
public class Stock {
    private String stockName;
    private Double threshold;
    private Double price = Double.valueOf(0.0);

    public void setStockName(String stockName) {
        this.stockName = stockName;
    }

    public void setThreshold(double aThreshold) {
        this.threshold = aThreshold;
    }

    public void tradeAt(double aPrice) {
        price = aPrice;
        evaluateBarrier(aPrice);
    }

    public STATUS getStatus() {
        return status;
    }

    private STATUS status = STATUS.OFF;

    public enum STATUS {
        OFF, ON;
    }

    private void evaluateBarrier(Double aPrice) {
        if (aPrice.compareTo(threshold) >= 0) {
            status = STATUS.ON;
        } else {
            status = STATUS.OFF;
        }
    }
}
