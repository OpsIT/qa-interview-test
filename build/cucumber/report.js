$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("org/welch/test/sandbox/cucumber/interview/interview.feature");
formatter.feature({
  "id": "a-trader-is-alerted-to-status-changes",
  "description": "In order that I make a profit\r\nAs a person who likes to dabble in the stock market\r\nI want to monitor stock market prices against thresholds",
  "name": "A trader is alerted to status changes",
  "keyword": "Feature",
  "line": 1
});
formatter.scenario({
  "id": "a-trader-is-alerted-to-status-changes;;;2",
  "description": "",
  "name": "",
  "keyword": "Scenario Outline",
  "line": 14,
  "type": "scenario"
});
formatter.step({
  "name": "a stock of STK1",
  "keyword": "Given ",
  "line": 7,
  "matchedColumns": [
    0
  ]
});
formatter.step({
  "name": "a threshold of 20.0",
  "keyword": "And ",
  "line": 8,
  "matchedColumns": [
    1
  ]
});
formatter.step({
  "name": "the stock is traded with 5.0",
  "keyword": "When ",
  "line": 9,
  "matchedColumns": [
    2
  ]
});
formatter.step({
  "name": "the alert status will be OFF",
  "keyword": "Then ",
  "line": 10,
  "matchedColumns": [
    3
  ]
});
formatter.match({
  "arguments": [
    {
      "val": "STK1",
      "offset": 11
    }
  ],
  "location": "InterviewStepDefs.a_stock_of(String)"
});
formatter.result({
  "duration": 116681976,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "20.0",
      "offset": 15
    }
  ],
  "location": "InterviewStepDefs.a_threshold_of(double)"
});
formatter.result({
  "duration": 631174,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "5.0",
      "offset": 25
    }
  ],
  "location": "InterviewStepDefs.the_stock_is_traded_with_(double)"
});
formatter.result({
  "duration": 75433,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "OFF",
      "offset": 25
    }
  ],
  "location": "InterviewStepDefs.the_alert_status_will_be_OFF(String)"
});
formatter.result({
  "duration": 4315074,
  "status": "passed"
});
formatter.scenario({
  "id": "a-trader-is-alerted-to-status-changes;;;3",
  "description": "",
  "name": "",
  "keyword": "Scenario Outline",
  "line": 15,
  "type": "scenario"
});
formatter.step({
  "name": "a stock of STK1",
  "keyword": "Given ",
  "line": 7,
  "matchedColumns": [
    0
  ]
});
formatter.step({
  "name": "a threshold of 20.0",
  "keyword": "And ",
  "line": 8,
  "matchedColumns": [
    1
  ]
});
formatter.step({
  "name": "the stock is traded with 10.0",
  "keyword": "When ",
  "line": 9,
  "matchedColumns": [
    2
  ]
});
formatter.step({
  "name": "the alert status will be OFF",
  "keyword": "Then ",
  "line": 10,
  "matchedColumns": [
    3
  ]
});
formatter.match({
  "arguments": [
    {
      "val": "STK1",
      "offset": 11
    }
  ],
  "location": "InterviewStepDefs.a_stock_of(String)"
});
formatter.result({
  "duration": 83130,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "20.0",
      "offset": 15
    }
  ],
  "location": "InterviewStepDefs.a_threshold_of(double)"
});
formatter.result({
  "duration": 70815,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "10.0",
      "offset": 25
    }
  ],
  "location": "InterviewStepDefs.the_stock_is_traded_with_(double)"
});
formatter.result({
  "duration": 64144,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "OFF",
      "offset": 25
    }
  ],
  "location": "InterviewStepDefs.the_alert_status_will_be_OFF(String)"
});
formatter.result({
  "duration": 51829,
  "status": "passed"
});
formatter.scenario({
  "id": "a-trader-is-alerted-to-status-changes;;;4",
  "description": "",
  "name": "",
  "keyword": "Scenario Outline",
  "line": 16,
  "type": "scenario"
});
formatter.step({
  "name": "a stock of STK1",
  "keyword": "Given ",
  "line": 7,
  "matchedColumns": [
    0
  ]
});
formatter.step({
  "name": "a threshold of 20.0",
  "keyword": "And ",
  "line": 8,
  "matchedColumns": [
    1
  ]
});
formatter.step({
  "name": "the stock is traded with 15.0",
  "keyword": "When ",
  "line": 9,
  "matchedColumns": [
    2
  ]
});
formatter.step({
  "name": "the alert status will be OFF",
  "keyword": "Then ",
  "line": 10,
  "matchedColumns": [
    3
  ]
});
formatter.match({
  "arguments": [
    {
      "val": "STK1",
      "offset": 11
    }
  ],
  "location": "InterviewStepDefs.a_stock_of(String)"
});
formatter.result({
  "duration": 70302,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "20.0",
      "offset": 15
    }
  ],
  "location": "InterviewStepDefs.a_threshold_of(double)"
});
formatter.result({
  "duration": 80564,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "15.0",
      "offset": 25
    }
  ],
  "location": "InterviewStepDefs.the_stock_is_traded_with_(double)"
});
formatter.result({
  "duration": 69275,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "OFF",
      "offset": 25
    }
  ],
  "location": "InterviewStepDefs.the_alert_status_will_be_OFF(String)"
});
formatter.result({
  "duration": 50289,
  "status": "passed"
});
formatter.scenario({
  "id": "a-trader-is-alerted-to-status-changes;;;5",
  "description": "",
  "name": "",
  "keyword": "Scenario Outline",
  "line": 17,
  "type": "scenario"
});
formatter.step({
  "name": "a stock of STK1",
  "keyword": "Given ",
  "line": 7,
  "matchedColumns": [
    0
  ]
});
formatter.step({
  "name": "a threshold of 20.0",
  "keyword": "And ",
  "line": 8,
  "matchedColumns": [
    1
  ]
});
formatter.step({
  "name": "the stock is traded with 19.9",
  "keyword": "When ",
  "line": 9,
  "matchedColumns": [
    2
  ]
});
formatter.step({
  "name": "the alert status will be OFF",
  "keyword": "Then ",
  "line": 10,
  "matchedColumns": [
    3
  ]
});
formatter.match({
  "arguments": [
    {
      "val": "STK1",
      "offset": 11
    }
  ],
  "location": "InterviewStepDefs.a_stock_of(String)"
});
formatter.result({
  "duration": 61578,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "20.0",
      "offset": 15
    }
  ],
  "location": "InterviewStepDefs.a_threshold_of(double)"
});
formatter.result({
  "duration": 59013,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "19.9",
      "offset": 25
    }
  ],
  "location": "InterviewStepDefs.the_stock_is_traded_with_(double)"
});
formatter.result({
  "duration": 87749,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "OFF",
      "offset": 25
    }
  ],
  "location": "InterviewStepDefs.the_alert_status_will_be_OFF(String)"
});
formatter.result({
  "duration": 44644,
  "status": "passed"
});
formatter.scenario({
  "id": "a-trader-is-alerted-to-status-changes;;;6",
  "description": "",
  "name": "",
  "keyword": "Scenario Outline",
  "line": 18,
  "type": "scenario"
});
formatter.step({
  "name": "a stock of STK1",
  "keyword": "Given ",
  "line": 7,
  "matchedColumns": [
    0
  ]
});
formatter.step({
  "name": "a threshold of 20.0",
  "keyword": "And ",
  "line": 8,
  "matchedColumns": [
    1
  ]
});
formatter.step({
  "name": "the stock is traded with 20.0",
  "keyword": "When ",
  "line": 9,
  "matchedColumns": [
    2
  ]
});
formatter.step({
  "name": "the alert status will be ON",
  "keyword": "Then ",
  "line": 10,
  "matchedColumns": [
    3
  ]
});
formatter.match({
  "arguments": [
    {
      "val": "STK1",
      "offset": 11
    }
  ],
  "location": "InterviewStepDefs.a_stock_of(String)"
});
formatter.result({
  "duration": 60552,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "20.0",
      "offset": 15
    }
  ],
  "location": "InterviewStepDefs.a_threshold_of(double)"
});
formatter.result({
  "duration": 72354,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "20.0",
      "offset": 25
    }
  ],
  "location": "InterviewStepDefs.the_stock_is_traded_with_(double)"
});
formatter.result({
  "duration": 65170,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "ON",
      "offset": 25
    }
  ],
  "location": "InterviewStepDefs.the_alert_status_will_be_OFF(String)"
});
formatter.result({
  "duration": 47210,
  "status": "passed"
});
formatter.scenario({
  "id": "a-trader-is-alerted-to-status-changes;;;7",
  "description": "",
  "name": "",
  "keyword": "Scenario Outline",
  "line": 19,
  "type": "scenario"
});
formatter.step({
  "name": "a stock of STK1",
  "keyword": "Given ",
  "line": 7,
  "matchedColumns": [
    0
  ]
});
formatter.step({
  "name": "a threshold of 20.0",
  "keyword": "And ",
  "line": 8,
  "matchedColumns": [
    1
  ]
});
formatter.step({
  "name": "the stock is traded with 25.0",
  "keyword": "When ",
  "line": 9,
  "matchedColumns": [
    2
  ]
});
formatter.step({
  "name": "the alert status will be ON",
  "keyword": "Then ",
  "line": 10,
  "matchedColumns": [
    3
  ]
});
formatter.match({
  "arguments": [
    {
      "val": "STK1",
      "offset": 11
    }
  ],
  "location": "InterviewStepDefs.a_stock_of(String)"
});
formatter.result({
  "duration": 60039,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "20.0",
      "offset": 15
    }
  ],
  "location": "InterviewStepDefs.a_threshold_of(double)"
});
formatter.result({
  "duration": 61065,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "25.0",
      "offset": 25
    }
  ],
  "location": "InterviewStepDefs.the_stock_is_traded_with_(double)"
});
formatter.result({
  "duration": 61065,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "ON",
      "offset": 25
    }
  ],
  "location": "InterviewStepDefs.the_alert_status_will_be_OFF(String)"
});
formatter.result({
  "duration": 49262,
  "status": "passed"
});
});