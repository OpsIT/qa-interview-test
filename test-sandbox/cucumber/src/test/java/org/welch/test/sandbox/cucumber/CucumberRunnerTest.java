package org.welch.test.sandbox.cucumber;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

/**
 * Created with IntelliJ IDEA.
 * User: matt
 * Date: 10/08/14
 * Time: 13:02
 * To change this template use File | Settings | File Templates.
 */
@RunWith(Cucumber.class)
@CucumberOptions(format = {"progress", "html:build/cucumber"},
        glue = "org.welch.test.sandbox.cucumber",
        features = "classpath:org/welch/test/sandbox/cucumber/interview")
public class CucumberRunnerTest {
}
